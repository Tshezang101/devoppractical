const mongoose = require('mongoose')
const User = require('./../models/userModel')

exports.getAllUsers = async(req, res, next) => {
    try{
        const users = await User.find()
        res.status(200).json({data:users, status: 'success'})
    }catch (err) {
        res.status(500).json({error: err.message});
    }
}

exports.createUsers = async(req, res) => {
    try{
        const users = await User.create(req.body)
        // console.log(req.body.name)
        res.json({data:users, status: 'success'})
    }catch (err) {
        res.status(500).json({error: err.message});
    }
}

exports.getUser = async(req, res) => {
    try{
        const users = await User.findById(req.params.id);
        res.status(200).json({dtaa:users, status: "success"})
    }catch (err) {
        res.status(500).json({error: err.message});
    }
}

exports.updateUser = async(req, res) => {
    try{
        const users = await User.findByIdAndUpdate(req.params.id, req.body);
        res.status(200).json({data:users, status: 'success'})
    }catch (err) {
        res.status(500).json({error: err.message});
    }
}
exports.deleteUser = async(req, res) => {
    try{
        const users = await User.findByIdDelete(req.params.id);
        res.status(200).json({data:users, status: 'success'})
    }catch (err) {
        res.status(500).json({error: err.message});
    }
}