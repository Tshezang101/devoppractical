const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please enter your name!'],
    },
    email: {
        type: String,
        required: [true, 'Please enter your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Plaese provide a valid email']
    },
    photo: {
        type: String,
        default: 'default.jpg',
    },
    role: {
        type: String,
        enum: ['user', 'sme', 'pharmacist', 'admin'],
        default: 'user',
    },
    password: {
        type: String,
        required: [true, 'Please enter your password!'],
        minlength: 8,
        select: false,
    },
    passwordConfirm: {
        type: String,
        required: [true, 'Please confirm your password'],
        validate: {
            // this only works on SAVE!!
            validator: function (el) {
                return el === this.password
            },
            message: "Passwords are not the same"
        },
    },
    active: {
        type: Boolean,
        default: true,
        select: false,
    },
})
// middleware
userSchema.pre('save',async function (next){ // encrypt the password
    // only run this function if password was actually modified
    if(!this.isModified('password')) return next()

    // hash the password with cost of 12
    this.password = await bcrypt.hash(this.password, 12)
    // delete passwordconfirm field
    this.passwordConfirm = undefined
    next()
    
})

userSchema.pre('findOneAndUpdate', async function(next){
    const update = this.getUpdate();
    if (update.password !== '' &&
        update.password !== undefined &&
        update.password == update.passwordConfirm){
       
    //   hash thye password with cost of 12
    this.getUpdate().password = await bcrypt.hash(update.password, 12)

    // delete passwordconfirm field / setting the field to undefined
    update.passwordConfirm = undefined
    next()
    }else
    next()
})

// tnstance method is availblr in all document of certain collentions
userSchema.methods.correctPassword = async function(
    candidatePassword,
    userPassword,
){
    return await bcrypt.compare(candidatePassword, userPassword)
}

const User = mongoose.model('User', userSchema)
module.exports = User