const express = require('express');
const userController = require('./../controllers/userController');
const authController = require('./../controllers/authController');
const viewController = require('./../controllers/viewController');

const router = express.Router()

router.get('/', viewController.getHome)
router.get('/login', viewController.getLoginForm)
router.get('/signup', viewController.getSignupForm)


router.post('/signup', authController.signup)

router
    .route('/')
    .get(userController.getAllUsers)
    .post(userController.createUser)

    router
        .route('/:id')
        .get(userController.getUser)
        .patch(userController.updateUser)
        .delete(userController.deleteUser)
        
    module.exports = router