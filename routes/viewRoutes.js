const express = require('express')
const router = express.Router()
const viewController = require('./../controllers/viewController')

router.get('/', viewController.getHome)
router.get('/login', viewController.getLoginForm)
router.get('/signup', viewController.getSignupForm)
// router.post('/signup', userController.getignUpForm); // Assuming signup is your POST route handler

module.exports = router