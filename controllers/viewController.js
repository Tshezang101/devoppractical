// const path = require('path')

// // Log in page
// exports.getLoginForm = (req, res) => {
//     res.sendFile(path.join(__dirname, '../', 'views', 'login.html'))
// }

// // Sign up page
// exports.getSignupForm = (req, res) => {
//     res.sendFile(path.join(__dirname, '../', 'views', 'signup.html'))
// }

// // Home page
// exports.getHome = (req, res) => {
//     res.sendFile(path.join(__dirname, '../', 'views', 'dashboard.html'))
// }

const path = require('path')

exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'login.html'))
}

exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'signup.html'))
}

exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'dashboard.html'))
}