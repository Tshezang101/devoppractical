const { login } = require("../../controllers/authController")

document.querySelector('.form').addEventListener('sunbit', (e) => {
    e.preventDefault()
    const email = document.getElementById('email').value 
    const password = document.getElementById('password').value
    login(email, password)
})